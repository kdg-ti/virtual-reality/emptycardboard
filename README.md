# EMPTY CARDBOARD PROJECT

Je kan dit project gebruiken als template voor je eigen cardboard projecten

- Maak een nieuwe folder aan met de naam van je project
- Kopieer de Assets, Packages en ProjectSettings folder van dit project over naar die nieuwe folder
- Open je nieuw project in Unity
- Pas aan:
  - Player Settings --> Product Name
  - Player Settings --> Other Settings --> Package Name
